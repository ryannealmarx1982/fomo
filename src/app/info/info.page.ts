import { Component, OnInit } from '@angular/core';
import { ServerService } from '../service/server.service';
import { ToastController,NavController,Platform,LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  data:any;
  text:any;
  hasFav = false;
  constructor(public server : ServerService,public toastController: ToastController,private nav: NavController,public loadingController: LoadingController) {

   this.data = JSON.parse(localStorage.getItem('menu_item'));
   this.text = JSON.parse(localStorage.getItem('app_text'));
   this.hasFav = this.data.wish == 1 ? true : false;

   console.log(this.data);
  }

  ngOnInit() 
  {
  	
  }

  wishList(id)
  {
    if(localStorage.getItem('user_id') && localStorage.getItem('user_id') != "null")
    {
      this.addWish(id);
    }
    else
    {
      this.nav.navigateForward('/login');      
    }
  }

  async addWish(id)
  {
     this.hasFav = this.hasFav == true ? false : true;

     if(this.hasFav)
     {
       this.presentToast("Added to Favorites");
     }
     else
     {
       this.presentToast("Removed from Favorites");
     }

    this.server.addWish(localStorage.getItem('user_id'),id).subscribe((response:any) => {
    
    });
  }

  async book(data)
  {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

    this.server.book(this.data.email,data).subscribe((response:any) => {
    
    this.presentToast("Thanks! We have received your query. We will contact you soon.");

    loading.dismiss();

    });
  }

  async presentToast(txt) {
    const toast = await this.toastController.create({
      message: txt,
      duration: 3000,
      position : 'top',
      mode:'ios',
      color:'dark'
    });
    toast.present();
  }

}
