import { Component, OnInit,ViewChild } from '@angular/core';
import { ServerService } from '../service/server.service';
import { NavController,Platform,LoadingController,IonSlides,Events } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

showWish = false;
BannerOption = {
    initialSlide: 0,
    slidesPerView: 2.3,
    loop: true,
    centeredSlides: false,
    autoplay:false,
    speed: 500,
    spaceBetween:7,

  }

  SearchOption = {
    initialSlide: 0,
    slidesPerView: 3.5,
    loop: false,
    centeredSlides: false,
    autoplay:false,
    speed: 500,
    spaceBetween:-20,

  }

  TrendOption = {
    initialSlide: 0,
    slidesPerView: 1.4,
    loop: true,
    centeredSlides: false,
    autoplay:false,
    speed: 800,
    spaceBetween:-9,

  }

  MiddleBannerOption = {
    initialSlide: 0,
    slidesPerView: 1.3,
    loop: false,
    centeredSlides: false,
    autoplay:true,
    speed: 800,
    spaceBetween:7,

  }

  city_name:any;
  data:any;
  fakeData = [1,2,3,4,5,6,7];
  oldData:any;
  showLoading = false;
  filterPress:any;
  hasSearch = false;
  searchQuery:any;
  count:any;
  text:any;
  wishArray:any = [];
  
  allStoreData:any = [];
  StoreData:any = [];
  
  constructor(public server : ServerService,private nav: NavController,public events: Events,private platform: Platform)
  {
    
  }

  ionViewWillEnter()
  {
    if(localStorage.getItem('app_text'))
    {
      this.text = JSON.parse(localStorage.getItem('app_text'));
    }

    this.city_name = localStorage.getItem('city_name');
	
  
    this.server.cartCount(localStorage.getItem('cart_no')).subscribe((response:any) => {

      this.count = response.data;
	  

     });

  
  
    
  }

  ngOnInit()
  {
    this.searchQuery = null;
    this.hasSearch   = false;

	
	this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng'));
	
  }

  nearBy()
  {
    this.data = null;
    this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng'));
  }

  async loadData(city_id)
  {
    this.data = null;
    
    var lid = localStorage.getItem('lid') ? localStorage.getItem('lid') : 0;

    this.server.homepage(city_id+"&lid="+lid).subscribe((response:any) => {

    this.data = response.data;
    this.text = response.data.text;
	
	this.allStoreData = this.data.store;

	for (let i = 0; i < 10; i++) { 
      this.StoreData.push(this.data.store[i]);
    }

	
    for(var i =0;i<this.data.store.length;i++)
    {
      if(this.data.store[i].wish == 1)
      {
        this.wishArray.push(this.data.store[i].id);
      }
    }

    this.events.publish('text', this.text);

    localStorage.setItem('app_text', JSON.stringify(response.data.text));
	


    });
	
  }

  
  loadMoreData(event) {
  
    setTimeout(() => {
      
    var next = (this.StoreData.length*1)+(10*1);

      for (let i = this.StoreData.length; i < next; i++) { 

        if(this.allStoreData[i])
        {
          this.StoreData.push(this.allStoreData[i]);

        }
      }

      event.target.complete();
 
      //console.log("data "+this.data.length);
      //console.log("Alldata "+this.allData.length);

      if (this.StoreData.length == this.allStoreData.length || this.StoreData.length > this.allStoreData.length) {
        event.target.disabled = true;
      }
    }, 500);
  }
  
  
  
  
  search(ev)
  {
    var val = ev.target.value;

    if(val && val.length > 0)
    {
      this.data      = null;
      this.hasSearch = val;
	this.allStoreData = [];
	this.StoreData = [];
     
	  this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng')+"&q="+val);
    }
    else
    {
      this.ionViewWillEnter();
      this.hasSearch = false;
    }
  }
  
  
  search2(ev)
  {
    var val = ev.target.value;
	
    if(val && val.length > 0)
    {
      this.data      = null;
	  this.allStoreData = [];
	this.StoreData = [];
     
      this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng')+"&day="+val);
    }
    else
    {
      this.ionViewWillEnter();
	  
     
    }
  }

  async dataFilter(type)
  {
    this.showWish = false;
    this.filterPress = type;
    await this.delay(1000);
    this.filterPress = null;

    if(type == 1)
    {
      this.data.store.sort((a,b) => {
    
        return parseFloat(b.discount_value) - parseFloat(a.discount_value);

        });
    }
    else if(type == 2)
    {
      this.data.store.sort((a,b) => {
    
        return parseFloat(a.delivery_time) - parseFloat(b.delivery_time);

        });
    }
    else if(type == 3)
    {
      this.data.store.sort((a,b) => {
    
        return parseFloat(b.trending) - parseFloat(a.trending);

        });
    }
    else if(type == 4)
    {
        this.data.store.sort((a,b) => {
    
        return parseFloat(b.id) - parseFloat(a.id);

        });
    }
    else if(type == 5)
    {
      this.data.store.sort((a,b) => {
    
        return parseFloat(b.rating) - parseFloat(a.rating);

        });
    }
    else if(type == 6)
    {
      
    }
    else if(type == 7)
    {
      this.showWish = true;
      if(localStorage.getItem('user_id') && localStorage.getItem('user_id') != "null")
      {
        this.data.store.sort((a,b) => {
      
          return parseFloat(b.wish) - parseFloat(a.wish);

          });
      }
      else
      {
        this.nav.navigateForward('/login');      
      }
    }
  }


  async delay(ms: number) {
    
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  bannerLink(offer)
  {
    if(offer.link)
    {
      this.data = null;
      //this.loadData(localStorage.getItem('city_id'));
	  this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng')+"&banner="+offer.id);
    }
  }

  doRefresh(event) {

    this.loadData(localStorage.getItem('city_id')+"?lat="+localStorage.getItem('current_lat')+"&lng="+localStorage.getItem('current_lng'));

    setTimeout(() => {
      
      event.target.complete();
    }, 2000);
  }

  itemPage(storeData)
  {
    localStorage.setItem('menu_item', JSON.stringify(storeData));
    
    this.nav.navigateForward('/info');
  }

  wishList(id)
  {
    if(localStorage.getItem('user_id') && localStorage.getItem('user_id') != "null")
    {
      this.addWish(id);
    }
    else
    {
      this.nav.navigateForward('/login');      
    }
  }

  async addWish(id)
  {
      if(this.wishArray.includes(id))
      {
        var ind = this.wishArray.indexOf(id);

        this.wishArray.splice(ind,1);
      }
      else
      {
        this.wishArray.push(id);
      }

     this.server.addWish(localStorage.getItem('user_id'),id).subscribe((response:any) => {
    
    });
  }

hasWish(id)
{
    if(this.wishArray.includes(id))
    {
      return true;
    }
    else
    {
      return false;
    }
}
}
